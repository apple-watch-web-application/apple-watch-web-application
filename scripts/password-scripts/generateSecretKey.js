const crypto = require('crypto');

// Generate a random secret key
const generateSecretKey = () => {
  const length = 32; // Length of the key in bytes

  // Generate a secure random buffer
  const buffer = crypto.randomBytes(length);

  // Convert the buffer to a hex string
  const secretKey = buffer.toString('hex');

  return secretKey;
};

// Generate and print the secret key
const secretKey = generateSecretKey();
console.log('Generated Secret Key:', secretKey);

