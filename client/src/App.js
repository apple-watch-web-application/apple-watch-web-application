import logo from './logo.svg';
import './App.css';
import { useState, useEffect } from 'react';

const BASE_URL = "https://cvatech.w3.uvm.edu"

function App() {
  const [isAuthenticated, setIsAuthenticated] = useState(false);
  const [isLoading, setIsLoading] = useState(true);
  const [selectedFiles, setSelectedFiles] = useState(null);
  const [password, setPassword] = useState("");
  const [fileName, setFileName] = useState("");
  const [isHealthy, setIsHealthy] = useState("");
  const [showHealth, setShowHealth] = useState(false);
  const [uploadRes, setUploadRes] = useState("")
  const [uploadCount, setUploadCount] = useState(0)
  const [sessionExpired, setSessionExpired] = useState(false)

  const fileSelectedHandler = (event) => {
    setSelectedFiles(event.target.files);
  };

  const passwordChangeHandler = (event) => {
    setPassword(event.target.value);
  };

    
  const performHealthCheck = async () => {
    try {
      const response = await fetch(`${BASE_URL}/?healthcheck=true`);
      const responseBody = await response.text();
      setIsHealthy(responseBody)
      setShowHealth(true)
    } catch (error) {
      setShowHealth(true)
      console.error('Error during health check:', error);
    }
  };

  const fileUploadHandler = async (e) => {
    e.preventDefault();
    if (!selectedFiles) {
      alert("reselect files before submitting")
      setSelectedFiles(null)
      setFileName(null)
      return;
    }
    const formData = new FormData();
    Array.from(selectedFiles).forEach(file => {
      formData.append(file.name, file);
    });    
    const response = await fetch(`${BASE_URL}/?password=${password}`, {
      method: 'POST',
      body: formData,
    });
    let uploadResponse = await response.text()
    if (uploadResponse == "Files uploaded successfully") {
      setUploadCount(uploadCount + 1)
    }
    console.log(uploadResponse)
    setUploadRes(uploadResponse)
    setSelectedFiles(null)
    setFileName("")
  };
  
  const fileNameChangeHandler = (event) => {
    setFileName(event.target.value);
  };
  const downloadFile = async (name) => {
    const fetchOptions = {
      headers: {
        'Authorization': localStorage.getItem('swapna-token')
      },
    };
    const response = await fetch(`${BASE_URL}/?filename=` + encodeURIComponent(name), fetchOptions);
    if (!response.ok) {
      console.error('Error downloading file:', await response.text());
      return;
    }
    const blob = await response.blob();
    const url = window.URL.createObjectURL(blob);
    const link = document.createElement('a');
    link.href = url;
    link.download = name;
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  }

  const downloadFileHandler = async (e) => {
    e.preventDefault();
    downloadFile(fileName)
  };

  const Good = () => <span>✅</span>
  const Bad = () => <span>❌</span>

  const handleLogin = (event) => {
    event.preventDefault();

    const formData = new FormData(event.target);
    const username = formData.get('username');
    const password = formData.get('password');

    setIsLoading(true);

    fetch(`${BASE_URL}/?login=true`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ username, password }),
    })
      .then((response) => {
        if (response.ok) {
          console.log("login ok")
          return response.json();
        } else {
          console.log("login failed", JSON.stringify(response))

          throw new Error('Login failed');
        }
      })
      .then((data) => {
        if (data.token) {
          console.log("setting token")

          localStorage.setItem('swapna-token', data.token);
          localStorage.setItem('swapna-token-time', Date.now());
          setIsAuthenticated(true);
        } else {
          console.log("no token in data", data)
          setIsAuthenticated(false);
          console.error('Authentication failed');
        }
        setIsLoading(false);
      })
      .catch((error) => {
        console.log("catch")
        console.error('Error occurred during login:', error);
        setIsAuthenticated(false);
        setIsLoading(false);
      });
  };

  useEffect(() => {
    fetch(`${BASE_URL}/?isAuth=true`, {
      method: 'GET',
      headers: {
        'Authorization': localStorage.getItem('swapna-token')
      }
    })
      .then((response) => {
        if (response.ok) {
          setIsAuthenticated(true);
        } else {
          setIsAuthenticated(false);
        }
        setIsLoading(false);
      })
      .catch((error) => {
        console.error('Error occurred during authentication:', error);
        setIsAuthenticated(false);
        setIsLoading(false);
      });
  }, []);

  useEffect(() => {
    document.title = 'Swapna Project';
  }, []);

  const handleLogout = (isLoggedIn) => {
    setIsAuthenticated(isLoggedIn);
  };
  
 if (isLoading) {
    return <div className="App-header"><div>Loading...</div></div>;
  }

  if (!isAuthenticated) {
    // Render login form
    return (
      <div className="App">
      <div className="App-header">
        <h1>Login</h1>
        <form onSubmit={handleLogin}>
          <div className="login-form">
            <input type="text" name="username" placeholder="Username" />
            <input type="password" name="password" placeholder="Password" />
            <button type="submit">Login</button>
          </div>
        </form>
      </div>
      </div>
    );
  }


  return (
    <div className="App">
      {sessionExpired && <div className='continue-session-modal'><ContinueSessionModal setSessionExpired={setSessionExpired} /></div>}
      <header className="App-header">
        <div className="nav">
          <h1>Silkspace Tech Demo</h1>
          <LogoutButton onLogout={handleLogout} />
        </div>
        <CountdownTimer setSessionExpired={setSessionExpired} />
        <section>
          <div>
            <h2>
              Health Check
            </h2>
            <p>
             On click, a green check displays if systems are up. A red X means something is wrong.
            </p>

            <button className='health-btn action-btn' onClick={performHealthCheck}>Perform Health Check</button>
            {
              isHealthy === "healthy" && showHealth && <Good/>
            }
            {
              isHealthy !== "healthy" && showHealth && <Bad/>
            }
          </div>
          <section>
            <FileLisTable uploadCount={uploadCount} downloadFileHandler={downloadFile} />
          </section>
          <div className="upload-form">
            <h2>
              Upload API Demo
            </h2>
            <p>If the upload was successful, a green check will display. If the upload failed, a red X will display</p>
            <div className='upload-files-container'>
              <input type="file" multiple onChange={fileSelectedHandler} />
              <div className='file-select-submit'>
              <input type="password" onChange={passwordChangeHandler} placeholder='enter secret key' />
              <button className='action-btn' onClick={fileUploadHandler}>Upload</button>
            </div>

            {
              uploadRes === "error" && <Bad/>
            }
            {
              uploadRes === "Files uploaded successfully" && <Good/>
            }
            {
              uploadRes === "Unauthorized" && <span>Incorrect Secret Key</span>
            }
          </div>
          </div>
          <div className="download-form">
            <h2>
             Download API demo
            </h2>
            <p>Download form demos downloading a single file present on the server</p>
            <div className='file-select-submit'>
              <input type="text" onChange={fileNameChangeHandler} placeholder='file name with extension' />
              <button className='action-btn' onClick={downloadFileHandler}>Download</button>
            </div>
          </div>
        </section>
      </header>
    </div>
  );
}

function FileLisTable({ downloadFileHandler, uploadCount }) {
  const [files, setFiles] = useState([]);
  const BASE_URL = "https://cvatech.w3.uvm.edu"
  const [selectedFiles, setSelectedFiles] = useState([]);
  const [toastMessage, setToastMessage] = useState('');
  const [toastType, setToastType] = useState('');
  const [show, setShow] = useState(false);
  const [passkey, setPasskey] = useState('');
  const [filterTerm, setFilterTerm] = useState('');
  const [displayedFiles, setDisplayedFiles] = useState([]);
  const [sortType, setSortType] = useState("")
  const handleFilterChange = event => {
    console.log("change filter")
    setFilterTerm(event.target.value);
  }

  const showToast = (message, type) => {
    setToastMessage(message);
    setToastType(type);
    setShow(true);
  };

  const parseDateTimeFromName = (name) => {
    try {
      let timeStampPattern = /(\d{2}-\d{2}-\d{4}_\d{2}(:?\d{2}){2})/; // Updated RegExp to capture both formats
      let timeStamp = name.match(timeStampPattern)[0];
  
      let [date, time] = timeStamp.split('_');
  
      // Split date into components
      let [month, day, year] = date.split('-').map(Number);  // Convert to numbers
  
      // Check if time has colon
      if (time.includes(':')) {
        // Split time into components
        let [hour, minute, second] = time.split(':').map(Number);
        let dateTime = new Date(year, month - 1, day, hour, minute, second);  // month is 0-based
        return dateTime;
      } else {
        // Split time into components and add leading zeros if necessary
        let hour = Number(time.slice(0, 2));
        let minute = Number(time.slice(2, 4));
        let second = Number(time.slice(4, 6));
        let dateTime = new Date(year, month - 1, day, hour, minute, second);  // month is 0-based
        return dateTime;
      }
    } catch (e) {
      return "Invalid-Date"
    }
  }
  

  const handleClose = () => {
    setShow(false);
  };

  const handlePasskeyChange = (event) => {
    setPasskey(event.target.value);
  };

// Filter and sort files array.
useEffect(() => {

  let processedFiles = files;

  // Apply filter if filterTerm is not empty.
  if (filterTerm) {
    processedFiles = processedFiles.filter(file => file.name.toLowerCase().includes(filterTerm.toLowerCase()));
  }

  if (sortType == "file") {
    console.log("sort by file ")
    processedFiles = processedFiles.sort((file1, file2) => {
      return file1.name.toLowerCase().localeCompare(file2.name.toLowerCase())
    });
  }

  if (sortType == "date") {
    processedFiles = processedFiles.sort((file1,file2) => {
      let file1Date = parseDateTimeFromName(file1.name)
      let file2Date = parseDateTimeFromName(file2.name)
      return file1Date - file2Date; // Subtraction instead of '<' operator
    })
  }

  setDisplayedFiles([...processedFiles]);
}, [files, filterTerm, sortType]);


  useEffect(() => {
    const fetchOptions = {
      headers: {
        'Authorization': localStorage.getItem('swapna-token')
      },
    };
    fetch(`${BASE_URL}/?allFiles=true`, fetchOptions)
      .then(response => response.json())
      .then(data => setFiles(data))
      .catch(error => console.error('Error fetching files:', error));
  }, [uploadCount]);

  const handleFileSelect = (file) => {
    if (selectedFiles.includes(file)) {
      setSelectedFiles(selectedFiles.filter(selectedFile => selectedFile !== file));
    } else {
      setSelectedFiles([...selectedFiles, file]);
    }
  };

  const handleDownloadSelected = () => {
    downloadBatchFiles(selectedFiles.map(file => file.name), showToast)
  };

  const handleDeleteSelected = () => {
    deleteBatchFiles(selectedFiles.map(file => file.name), showToast)
  };

  const handleSortChange = (e) => {
    console.log(e.target.value)
    setSortType(e.target.value)
  }

  const deleteBatchFiles = (fileNames, showToast) => {
    const password = passkey; // Replace with your actual password
  
    const queryParams = new URLSearchParams({
      password,
      fileNames: fileNames.join(','),
    });
  
    const requestOptions = {
      method: 'PUT',
    };
  
    fetch(`https://cvatech.w3.uvm.edu/?${queryParams.toString()}`, requestOptions)
      .then((response) => response.json())
      .then((data) => {
        if (data.success) {
          showToast('Files deleted successfully', 'success');
        } else {
          showToast(`Error deleting files: ${data.errorFiles.join(', ')}`, 'error');
        }
      })
      .catch((error) => {
        showToast('Error occurred during file deletion', 'error');
        console.error('Error deleting files:', error);
      });
  };
  
  

  const downloadBatchFiles = (fileNames, callback) => {
    const query = `batchFiles=${fileNames.join(',')}`;
    const url = `https://cvatech.w3.uvm.edu/?${query}`;
    const fetchOptions = {
      headers: {
        'Authorization': localStorage.getItem('swapna-token')
      },
    };
    fetch(url, fetchOptions)
      .then(response => {
        if (!response.ok) {
          throw new Error(`HTTP error! Status: ${response.status}`);
        }
        return response.blob();
      })
      .then(blob => {
        // Handle the response blob as needed
        // For example, you can create a download link and trigger the download
        const downloadLink = URL.createObjectURL(blob);
        const anchorElement = document.createElement('a');
        anchorElement.href = downloadLink;
        anchorElement.download = 'batch_download.zip';
        anchorElement.click();
        setSelectedFiles([])
        callback('Download completed successfully', 'success');
      })
      .catch(error => {
        callback('Error occurred during download', 'error');
        console.error('Error making fetch request:', error);
      });
  };
  

  return (
    <div>
      <h1>Files</h1>
      <p>
      {`Files should be named in the following format: datasource_<hyphen-seperated-identifier>_<month-day-year>_<hr:min:sec>.csv`}
      </p>
      <p>
      {`Please review the documentation for more details on file names`}
      </p>
      {show && <Toast message={toastMessage} type={toastType} handleClose={handleClose} />}
      {
        files.length === 0 && (
          <h3>No Files Found</h3>
        )
      }
      {
        files.length > 0 && (
          <div>
            <div className='table-controls'>
            <div class="table-control file-select-submit">
              <p>Filter</p>
              <input
                type="text"
                value={filterTerm}
                onChange={handleFilterChange}
                placeholder="Filter by file name"
              />
            </div>
            <div class="table-control-select">
              <p>Sort</p>
              <div className='select-container'>
                <select value={sortType} onChange={handleSortChange}>
                  <option value="">--Please choose an option--</option>
                  <option value="file">File Name</option>
                  <option value="date">Device File Creation Date</option>
                </select>
              </div>
            </div>
            <div className='table-button-container'>
              <div className='table-button'>
                <button className='action-btn' onClick={handleDownloadSelected}>Download Selected</button>
              </div>
            </div>
            </div>
            <div>
              <div>
                <table>
                  <thead>
                    <tr>
                      <th scope="col"></th>
                      <th scope="col">File Name</th>
                      <th scope="col">
                        Device File Creation Date
                      </th>
                      <th scope="col" className="uploaded-at">
                        Server File Write Date
                      </th>
                      <th scope="col" className="uploaded-at">
                        <span>Download</span>
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    {displayedFiles.map((file, index) => (
                      <tr key={index} className='data-row' onClick={() => handleFileSelect(file)}>
                        <td>
                          <input
                            type="checkbox"
                            checked={selectedFiles.includes(file)}
                            onChange={() => handleFileSelect(file)}
                          />
                        </td>
                        <td>
                          {file.name}
                        </td>
                        <td>
                          {
                            parseDateTimeFromName(file.name).toLocaleDateString && (
                              parseDateTimeFromName(file.name)?.toLocaleDateString() + " " + parseDateTimeFromName(file.name)?.toLocaleTimeString() 
                            )
                          }
                        </td>
                        <td className="uploaded-at">
                          {new Date(file.uploadedAt)?.toLocaleString()}
                        </td>
                        <td className="uploaded-at download-btn-container">
                          <button className='download-btn action-btn' onClick={(event) => {
                            event.stopPropagation()
                            downloadFileHandler(file.name)
                            }}>
                            <span>&#8595;</span>
                          </button>
                        </td>
                      </tr>
                    ))}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        )
      }

    </div>
  )
}

function Toast({ message, type, handleClose }) {
  return (
    <div className={`toast ${type}`}>
      <div className="toast-content">
        <div className="toast-message">
          <span className="centered-text">{message}</span>
        </div>
        <button className="toast-close" onClick={handleClose}>
          <strong>X</strong>
        </button>
      </div>
    </div>
  );
}

const LogoutButton = ({ onLogout }) => {
  const handleLogout = () => {
    localStorage.removeItem('swapna-token');
    onLogout(false); // Call the onLogout callback with the new authentication state
  };

  return (
    <button className='btn-logout' onClick={handleLogout}>
      Logout
    </button>
  );
};

const ContinueSessionModal = ({setSessionExpired}) => {
  const handleLogin = (event) => {
    event.preventDefault();

    const formData = new FormData(event.target);
    const username = formData.get('username');
    const password = formData.get('password');

    fetch(`${BASE_URL}/?login=true`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ username, password }),
    })
      .then((response) => {
        if (response.ok) {
          console.log("login ok")
          return response.json();
        } else {
          console.log("login failed", JSON.stringify(response))
          throw new Error('Login failed');
        }
      })
      .then((data) => {
        if (data.token) {
          console.log("setting token")
          localStorage.setItem('swapna-token', data.token);
          localStorage.setItem('swapna-token-time', Date.now());
        } else {
          console.log("no token in data", data)
          console.error('Authentication failed');
        }
      })
      .catch((error) => {
        console.log("catch")
        console.error('Error occurred during login:', error);
      });
  };

  return (
    <div>
      <h2>Your session is expired. Please sign in to continue.</h2>
      <form onSubmit={handleLogin}>
      <div className="login-form">
        <input type="text" name="username" placeholder="Username" />
        <input type="password" name="password" placeholder="Password" />
        <button type="submit">Login</button>
      </div>
    </form>
    </div>

  )
}

const CountdownTimer = ({setSessionExpired}) => {
  const [timeRemaining, setTimeRemaining] = useState(getTimeRemaining());

  function getTimeRemaining() {
    const tokenTime = localStorage.getItem('swapna-token-time');
    // 1 hour = 3600000 ms
    const tokenLifeTime = 3600000;
    // Time elapsed since the token was created
    const elapsedTime = Date.now() - tokenTime;
    // Time remaining (tokenLifeTime - elapsedTime)
    // Math.max is used to avoid negative time
    return Math.max(tokenLifeTime - elapsedTime, 0);
  }

  function formatTime(milliseconds) {
    let totalSeconds = Math.floor(milliseconds / 1000);
    let minutes = Math.floor(totalSeconds / 60);
    let seconds = totalSeconds % 60;
    if (minutes <= 0 && seconds <= 0) {
      setSessionExpired(true)
    }
    return `${minutes}m ${seconds}s`;
  }

  
  useEffect(() => {
    const intervalId = setInterval(() => {
      const remainingTime = getTimeRemaining();
      setTimeRemaining(remainingTime);
      if (remainingTime <= 0) {
        setSessionExpired(true);
      } else {
        setSessionExpired(false);
      }
    }, 1000); // Update every second

    return () => clearInterval(intervalId); // Cleanup interval on unmount
  }, [setSessionExpired]);
  

  return (
    <div>
      Time remaining in session: {formatTime(timeRemaining)}
    </div>
  );
};


export default App;
