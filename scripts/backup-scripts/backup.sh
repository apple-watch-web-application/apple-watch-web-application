#!/bin/bash

# Define the format for the timestamp
TIMESTAMP=$(date +"%m-%d-%Y-%H-%M-%S")

# Define the directories
DATA_DIR="./data"
BACKUP_DIR="./backups"

# Check if the backup directory exists, if not create it
if [ ! -d "$BACKUP_DIR" ]; then
  mkdir -p "$BACKUP_DIR"
fi

# Create a zip file with the current timestamp
zip -r "$BACKUP_DIR/data-$TIMESTAMP.zip" "$DATA_DIR"

echo "Backup of $DATA_DIR completed! The backup has been saved as data-$TIMESTAMP.zip"

