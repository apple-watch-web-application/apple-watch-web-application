const crypto = require('crypto');
// use the output of generateSecretKey as the salt param
function createHashedPassword(password, salt) {
    // Hash the password using the salt
    var hash = crypto.pbkdf2Sync(password, salt, 1000, 64, 'sha512').toString('hex');

    // The hashed password is returned
    return hash;
}

// Read password and salt from command line arguments
let password = process.argv[2]; // The first argument after the script name is assumed to be the password
let salt = process.argv[3]; // The second argument is assumed to be the salt

//example usage: node salt.js password salt
console.log(createHashedPassword(password, salt));
