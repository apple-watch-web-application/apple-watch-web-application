#!/bin/bash
set -e

# Copy app.js to the server
scp -oHostKeyAlgorithms=+ssh-dss ./app.js cvatech@w3.uvm.edu:/users/c/v/cvatech/www-root

# Restart the prod server
ssh -oHostKeyAlgorithms=+ssh-dss cvatech@w3.uvm.edu 'silk app cvatech.w3.uvm.edu load'

