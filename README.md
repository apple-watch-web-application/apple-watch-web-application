# Apple Watch Web Application Documentation

## Silk Space SSH Access

```
ssh -oHostKeyAlgorithms=+ssh-dss cvatech@w3.uvm.edu
```

You will be asked to enter the password for cvatech. Please contact the relavent UVM department for the password or provide a secure method for password sharing.

## Data Backup

The current system is set up to automatically create daily backups of a   **`/users/c/v/cvatech/www-root/data`** . These backups are stored in **`/users/c/v/cvatech/www-root/backups`**. Each backup is a compressed .zip file named with a timestamp indicating when the backup was created. This allows for easy identification of backups and restoration of data to a specific point in time.

A bash script, located at **`/users/c/v/cvatech/www-root/scripts/backup.sh`** handles the process of creating these backups.

The **`backup.sh`** script is scheduled to run automatically every day at 7:30 AM. This is achieved using a cron job. The cron job is set up in the system's crontab file. The cron line (**crontab -e**) in the crontab is as follows:

```swift
30 7 * * * /bin/bash /absolute/path/to/backup.sh
```

## Creating / Resetting Passwords

### **1. Creating a Hashed Password**

You will first need to create a hashed version of the password that you want to use. This can be achieved using a script as shown below:

Run the createHashedPassword.js script or equivalent: 

```jsx
const crypto = require('crypto');

function createHashedPassword(password, salt) {
    // Hash the password using the salt
    var hash = crypto.pbkdf2Sync(password, salt, 1000, 64, 'sha512').toString('hex');

    // The hashed password is returned
    return hash;
}

// Read password and salt from command line arguments
let password = process.argv[2]; // The first argument after the script name is assumed to be the password
let salt = process.argv[3]; // The second argument is assumed to be the salt

console.log(createHashedPassword(password, salt));
```

for example, as **`createHashedPassword.js`**. You can run it from the command line using Node.js as follows:

```jsx
node createHashedPassword.js <password> <salt>
```

Replace **`<password>`** with the password that you want to hash and **`<salt>`** with your chosen salt.

### ****2. Updating .silk.ini File****

After obtaining the hashed password, the next step is to add it to your **`.silk.ini`** file, along with the corresponding salt. The keys should be the username, and the username appended with **`_SALT`** respectively. ***Please note that the Silk Space server uses NGINX Unit, which handles env vars in a case-insensitive manner. Consequently, all usernames should be stored in uppercase letters.***

For existing users, you'd locate the existing environment variables for the user, and then update the hashed password and salt. Remember to use the uppercase version of the username when looking up these environment variables.

### ****3. Restarting the Silk Space Server****

After updating the **`.silk.ini`** file with the new hashed password and salt, you'll need to restart your Silk Space server for the changes to take effect. This can be done using the following command:

```jsx
silk app cvatech.w3.uvm.edu load
```

And that's it! You've either created a new user or reset an existing user's password on the Silk Space server. Please remember that due to NGINX Unit's handling of usernames, all usernames should be stored in uppercase.

## **Note on Environment Variables**

UVM's Silk Space servers may take some time (1 to 30 minutes) to add new environment variables, which can make the process of creating passwords a bit frustrating. Moreover, there seems to be a bug that prevents the last environment variable from being properly recognized in the **`.silk.ini`** file.

To work around this, we suggest adding a "buffer" environment variable at the end of all user and salt environment variables. If you find that the passwords aren't updating after 15 minutes, try changing this buffer variable's name or value and restart the server using the command described in the next step.

## **Simple Logging System in Node.js**

This server provides a utility function **`writeLog`** which allows for writing JSON data to a log file. Each log file is timestamped and labeled with a provided description for easy identification.

### **Usage**

The **`writeLog`** function requires two arguments:

1. **`titledesc`**: A string that describes the event that you're logging. This will be used in the filename of the log.
2. **`data`**: A JSON object containing the data that you want to log. This can be any information relevant to the event you're logging.

An example usage would be:

```jsx
writeLog('loginAttempt', { username: 'user1', result: 'Success' });
```

This would create a log file with a filename like **`loginAttempt-06-23-2023-15-34-56.txt`**

The log files are saved in a directory named **`/users/c/v/cvatech/www-root/logs`** in the same directory as your Node.js script. Ensure this directory exists before running your script, or modify the **`writeLog`** function to create the directory if it does not exist.

## **Architecture Overview**

The application follows a client-server architecture with JWT-based authentication for securing the server routes. The client-side is a React application, while the server-side uses a HTTP server powered by ****[NGINX Unit](https://www.npmjs.com/package/unit-http) as required by UVM Silkspace**.

**Note on Query Parameters vs REST Endpoints:**

In this application, query parameters are used for authentication and some specific functionality due to restrictions in the high-level server configurations on Silkspace. Query parameters allow passing additional information directly in the URL, making it accessible for the server to parse and utilize. Although it deviates from the standard RESTful practice of using specific endpoints for different resources and actions, query parameters offer a workaround in this particular setup.

By using query parameters, the application can pass necessary authentication information, such as the secret key, without relying on custom headers or complex configurations. It allows the server to perform the required authentication and authorization checks to ensure the security of the application. 

## ****Standard File Naming Format****

Files should follow this naming convention:

```
datasource_<hyphen-separated-identifier>_<month-day-year>_<hour:minute:second>.csv
```

In this convention, "datasource" represents the origin of the data, the "<hyphen-separated-identifier>" could be any series of alphanumeric strings separated by hyphens, the "<month-day-year>" corresponds to the date of data collection or processing, and "**hour:minute:second**" is the time of data collection or processing.

Examples of standard filenames include:

```
sensordata_ABC-CDE-4B20-1F1F-EBG45B_05-19-2023_18:49:54.csv
ios_ABC-CDE-4B20-1F1F-EBG45B_05-19-2023_18:49:54.csv
android_ABC-CDE-4B20-1F1F-EBG45B_05-19-2023_18:49:54.csv
```

## ****Non-standard File Naming Formats****

While we recommend adhering to some well-defined standard naming conventions, the system can also handle non-standard file names. These files should follow this general pattern:

```
<qualifier>_<month-day-year>_<hour:minute:second>.csv
```

The "<qualifier>" could be any series of alphanumeric characters and symbols, provided it doesn't mimic the date-time pattern ("**hour:minute:second**").

Colons in the time segment are optional. The system will handle files with or without them equally well.

Examples of non-standard filenames include:

```
some-qualifier_05-19-2023_18:49:54.csv
some-qualifier_05-19-2023_184954.csv
```

Remember, while non-standard file names can be accommodated, the most reliable way to ensure smooth processing is to use a single, standardized method of naming files.

### **Client-Side**

The client-side is responsible for rendering the user interface and handling user interactions. It communicates with the server-side API using various requests and includes JWT token authentication.

### Technologies Used:

- React (Front-end framework)
- localStorage (Web storage API for storing the JWT token)

### JWT Token Handling:

1. On each render, the client-side application checks for a JWT token in the **`localStorage`** named **`swapna-token`**.
2. If a token is present, the client makes a GET request to the server's **`/`** route with the query parameter **`isAuth=true`**, including the token in the **`Authorization`** header.
3. The server verifies the token's authenticity and expiration.
    - If the token is valid, the server responds with a success status code (200).
    - If the token is invalid or expired, the server responds with an unauthorized status code (401).
4. Based on the server's response, the client-side application renders the appropriate UI components.
    - If authorized, the client renders the main UI components.
    - If unauthorized, the client renders a login form to prompt the user to authenticate and obtain a valid token.

### Token Expiration:

The JWT token issued by the server has an expiration time of 1 hour (configurable). After the token expires, the user needs to reauthenticate to obtain a fresh token for continued access to the application.

# Server **Documentation**

## **Description**

The API server handles various requests from the client-side application. It provides functionality for user authentication, file management, and other operations. The API follows a RESTful design and includes the necessary endpoints to fulfill the client's requirements.

## **Endpoints**

### **1. GET `/`**

- Description: Returns the main HTML file for the client-side application.
- Method: GET
- Query Parameters:
    - None
- Response:
    - Status Code: 200 (OK)
    - Content-Type: text/html
    - Body: The main HTML file for the client-side application.

### **2. GET `/?isAuth=true`**

- Description: Verifies the authenticity of a JWT token.
- Method: GET
- Query Parameters:
    - **`isAuth`**: Set to **`true`** to check the token authentication.
- Headers:
    - **`Authorization`**: JWT token (Bearer token format)
- Response:
    - If the token is valid:
        - Status Code: 200 (OK)
        - Content-Type: text/plain
        - Body: "Authorized"
    - If the token is invalid or expired:
        - Status Code: 401 (Unauthorized)
        - Content-Type: text/plain
        - Body: "Unauthorized"

### **3. GET `/?allFiles=true`**

- Description: Retrieves a list of all files stored on the server.
- Method: GET
- Query Parameters:
    - **`allFiles`**: Set to **`true`** to fetch all files.
- Headers:
    - **`Authorization`**: JWT token (Bearer token format)
- Response:
    - If the token is valid:
        - Status Code: 200 (OK)
        - Content-Type: application/json
        - Body: JSON array containing the file information:
        
        ```jsx
        [
          {
            "name": "file1.txt",
            "uploadedAt": "2023-05-31T12:00:00Z"
          },
          {
            "name": "file2.png",
            "uploadedAt": "2023-05-31T12:15:00Z"
          },
          ...
        ]
        ```
        
        - If the token is invalid or expired:
            - Status Code: 401 (Unauthorized)
            - Content-Type: text/plain
            - Body: "Unauthorized"

### **4. GET `/?filename=<file_name>`**

- Description: Downloads a specific file from the server.
- Method: GET
- Query Parameters:
    - **`filename`**: The name of the file to download.
- Headers:
    - **`Authorization`**: JWT token (Bearer token format)
- Response:
    - If the token is valid and the file exists:
        - Status Code: 200 (OK)
        - Content-Type: application/octet-stream
        - Body: The file contents
    - If the token is invalid or expired:
        - Status Code: 401 (Unauthorized)
        - Content-Type: text/plain
        - Body: "Unauthorized"
    - If the file does not exist:
        - Status Code: 404 (Not Found)
        - Content-Type: text/plain
        - Body: "File not found"

### **5. GET `/?batchFiles=<file1,file2,...>`**

- Description: Downloads a batch of files as a ZIP archive.
- Method: GET
- Query Parameters:
    - **`batchFiles`**: Comma-separated list of file names to include in the batch.
- Headers:
    - **`Authorization`**: JWT token (Bearer token format)
- Response:
    - If the token is valid and all files exist:
        - Status Code: 200 (OK)
        - Content-Type: application/zip
        - Content-Disposition: attachment; filename=batch_download.zip
        - Body: The ZIP archive containing the requested files
    - If the token is invalid or expired:
        - Status Code: 401 (Unauthorized)
        - Content-Type: text/plain
        - Body: "Unauthorized"
    - If any of the files do not exist:
        - Status Code: 404 (Not Found)
        - Content-Type: text/plain
        - Body: "File not found"

### **6. POST `/?login=true`**

- Description: Authenticates the user and generates a JWT token.
- Method: POST
- Query Parameters:
    - **`login`**: Set to **`true`** to initiate the login process.
- Request Body:
    - Content-Type: application/json
    - Body:
    
    ```jsx
    {
      "username": "admin",
      "password": "password"
    }
    ```
    
    Response:
    
    - If the credentials are valid:
        - Status Code: 200 (OK)
        - Content-Type: application/json
        - Body:
        
        ```jsx
        {
          "token": "<JWT token>"
        }
        ```
        
        If the credentials are invalid:
        
        - Status Code: 401 (Unauthorized)
        - Content-Type: text/plain
        - Body: "Invalid credentials"

### 

### **7. POST `/?password=<secret_key>`**

- Description: Uploads files to the server.
- Method: POST
- Query Parameters:
    - **`password`**: The secret key for authentication.
- Response:
    - If the password is valid and the files are uploaded successfully:
        - Status Code: 200 (OK)
        - Content-Type: text/plain
        - Body: "Files uploaded successfully"
    - If the password is invalid:
        - Status Code: 401 (Unauthorized)
        - Content-Type: text/plain
        - Body: "Unauthorized"

Please note that for this endpoint, the authentication is done through the **`password`** query parameter instead of an authorization header. The server checks if the provided **`password`** matches the expected secret key stored in the environment variable **`NEW_KEY`**. If the passwords match, the files are uploaded successfully; otherwise, an **`Unauthorized`** response is returned.

### **8. PUT `/?password=<secret_key>&fileNames=<file1,file2,...>`**

- Description: Deletes specified files from the server.
- Method: PUT
- Query Parameters:
    - **`password`**: The secret key for authentication.
    - **`fileNames`**: Comma-separated list of file names to delete.
- Response:
    - If the password is valid and all files are deleted successfully:
        - Status Code: 200 (OK)
        - Content-Type: application/json
        - Body:
        
        ```jsx
        {
          "success": true
        }
        ```
        
        If the password is invalid or any files fail to delete:
        
        - Status Code: 401 (Unauthorized)
        - Content-Type: application/json
        - Body:
        
        ```jsx
        {
          "success": false,
          "errorFiles": ["file1.txt", "file2.png"]
        }
        ```
        

## **Deployment Process Documentation:**

This documentation provides instructions for deploying the client and server components of the application after making changes during the development process. Please follow the steps below to deploy your changes.

### **Client Deployment Process:**

1. Open the deployment script file named **`client_deploy.sh`** in a text editor.
2. Ensure that the script has the necessary execution permissions. If not, run the command **`chmod +x client_deploy.sh`** to grant the necessary permissions.
3. Update the source and destination paths in the **`scp`** command within the script. The source path should point to the build directory of your client application, and the destination path should specify the server location where the client files should be copied.
4. Save the changes to the script.
5. Open a terminal and navigate to the directory where the **`client_deploy.sh`** script is located.
6. Execute the deployment script by running the command **`./client_deploy.sh`**.
7. You will be prompted to enter the password for the client machine. Enter the password and press Enter.
8. The script will transfer the client files to the specified server location. Once the transfer is complete, you will be prompted to enter the password for the server machine. Enter the password and press Enter.
9. After entering the server password, the script will restart the production server. Wait for the process to complete.
10. Your client changes are now deployed and available on the server.

### **Server Deployment Process:**

1. Open the deployment script file named **`server_deploy.sh`** in a text editor.
2. Ensure that the script has the necessary execution permissions. If not, run the command **`chmod +x server_deploy.sh`** to grant the necessary permissions.
3. Update the source and destination paths in the **`scp`** command within the script. The source path should point to the location of your server's main JavaScript file (**`app.js`** or similar), and the destination path should specify the server location where the file should be copied.
4. Save the changes to the script.
5. Open a terminal and navigate to the directory where the **`server_deploy.sh`** script is located.
6. Execute the deployment script by running the command **`./server_deploy.sh`**.
7. You will be prompted to enter the password for the server machine. Enter the password and press Enter.
8. The script will transfer the server file to the specified server location. Once the transfer is complete, you will be prompted to enter the password for the server machine again. Enter the password and press Enter.
9. After entering the server password, the script will restart the production server. Wait for the process to complete.
10. Your server changes are now deployed and active on the server.

Note: When deploying both the client and server components, ensure that you have the necessary permissions to access and modify the relevant files. Additionally, be aware that you will be prompted to enter the password twice during the deployment process - once for the client machine and once for the server machine.

## WatchOS demo

```swift
//
//  ContentView.swift
//  Swapna Demo Watch App
//
//  Created by Sevan Golnazarian on 6/2/23.
//

import SwiftUI

import Foundation

func uploadCSVFiles(fileURLs: [URL], password: String) {
    var urlComponents = URLComponents(string: "https://cvatech.w3.uvm.edu/")!
    urlComponents.queryItems = [URLQueryItem(name: "password", value: password)]
    guard let url = urlComponents.url else {
        print("Invalid URL")
        return
    }
    
    
    var request = URLRequest(url: url)
    request.httpMethod = "POST"
    let boundary = UUID().uuidString
    request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")

    var data = Data()

    for fileURL in fileURLs {
        data.append("\r\n--\(boundary)\r\n".data(using: .utf8)!)
        let filename = fileURL.lastPathComponent
        data.append("Content-Disposition: form-data; name=\"\(filename)\"; filename=\"\(filename)\"\r\n".data(using: .utf8)!)
        data.append("Content-Type: text/csv\r\n\r\n".data(using: .utf8)!)
        do {
            let fileData = try Data(contentsOf: fileURL)
            data.append(fileData)
        } catch {
            print("Error reading file: \(error)")
            return
        }
    }

    data.append("\r\n--\(boundary)--\r\n".data(using: .utf8)!)

    let task = URLSession.shared.uploadTask(with: request, from: data) { (data, response, error) in
        if let error = error {
            print("Error uploading files: \(error)")
        } else if let data = data {
            let responseStr = String(data: data, encoding: .utf8)
            print("Received data:\n\(responseStr ?? "")")
        } else {
            print("No data received")
        }
    }
    task.resume()
}

func createDummyCSVFiles(deviceID: String) -> [URL]? {
    let csvText = "header1,header2\nrow1-1,row1-2\nrow2-1,row2-2\n"
    var fileURLs = [URL]()
    
    for i in 1...2 {
        let fileName = "\(deviceID)-\(Date().timeIntervalSince1970)-\(i).csv"
        guard let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else {
            print("Unable to get directory")
            return nil
        }
        let fileURL = dir.appendingPathComponent(fileName)
        
        do {
            try csvText.write(to: fileURL, atomically: true, encoding: .utf8)
            fileURLs.append(fileURL)
        } catch {
            print("Error creating file: \(error)")
            return nil
        }
    }
    
    return fileURLs
}

struct ContentView: View {
    let deviceID = "a234dbdegba"
    let password = "replace-with-secret-key"

    var body: some View {
        VStack {
            Image(systemName: "globe")
                .imageScale(.large)
                .foregroundColor(.accentColor)
            Text("Hello, world!")
            Button("Create and Upload CSVs") {
                guard let fileURLs = createDummyCSVFiles(deviceID: deviceID) else {
                    print("Error creating CSV files")
                    return
                }
                uploadCSVFiles(fileURLs: fileURLs, password: password)
            }
        }
        .padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
```