#!/opt/nodejs/v16/bin/node

const http = require('unit-http');
const fs = require('fs');
const path = require('path');
const url = require('url');
const querystring = require('querystring');
const formidable = require('formidable');
const archiver = require('archiver');
const jwt = require('jsonwebtoken');
const crypto = require('crypto');

require('dotenv').config();

const secretKey = process.env.JWT_SECRET; 

function verifyToken(token) {
  if (token) {
    try {
      jwt.verify(token, secretKey);
      return true;
    } catch (error) {
      console.error('Invalid JWT:', error);
    }
  }
  return false;
}

function getUsernameFromToken(token) {
  try {
      // Decode the token
      const decoded = jwt.verify(token, secretKey);
      // Return the username from the payload
      return decoded.username;
  } catch (error) {
      console.error('Could not extract username from token:', error);
      return null;
  }
}

function verifyPassword(user, userPassword, savedPasswordHash) {
  // Retrieve the salt from the env var
  const salt = process.env[`${user?.toUpperCase()}_SALT`];

  // Hash the password sent by the user using the retrieved salt
  var hash = crypto.pbkdf2Sync(userPassword, salt, 1000, 64, 'sha512').toString('hex');

  // The password is correct if the new hash matches the saved hash
  return savedPasswordHash === hash;
}

function verifyDeviceSecret(password, savedPasswordHash) {
    // Retrieve the salt from the env var
    const salt = process.env["DEVICE_SECRET"];

    // Hash the password sent by the user using the retrieved salt
    var hash = crypto.pbkdf2Sync(password, salt, 1000, 64, 'sha512').toString('hex');
  
    // The password is correct if the new hash matches the saved hash
    return savedPasswordHash === hash;
}

// Utility Function for Logging Text
function pad(number) {
  return number.toString().padStart(2, '0');
}

// writeLog('loginAttempt', { username: 'user1', result: 'Success' });
function writeLog(titledesc, data) {
  // Generate the current timestamp
  const now = new Date();
  const date = pad(now.getMonth() + 1) + "-" + pad(now.getDate()) + "-" + now.getFullYear();
  const time = pad(now.getHours()) + "-" + pad(now.getMinutes()) + "-" + pad(now.getSeconds());
  const timestamp = date + "-" + time;

  // Construct filename from titledesc and timestamp
  const filename = `${titledesc}-${timestamp}.txt`;

  // Define the path of the log file
  const logPath = path.join(__dirname, 'logs', filename);

  // Convert data to a string, assuming it's always a JSON object
  data = JSON.stringify(data, null, 2); // pretty print with 2 spaces indentation

  // Write data to the log file
  fs.writeFile(logPath, data, (err) => {
      if (err) {
          console.error(`Error writing log file: ${err}`);
      } else {
          console.log(`Log file ${filename} written successfully.`);
      }
  });
}

// Server Entrypoint
http.createServer(async function (req, res) {
  // Get the request origin
  const origin = req.headers.origin;
  res.setHeader('Access-Control-Allow-Origin', origin);
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, DELETE, PUT');
  res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization'); // Include Authorization header
  res.setHeader('Access-Control-Max-Age', '86400'); // 24 hours
  
  // Parse the URL of the incoming request
  const parsedUrl = url.parse(req.url, true); 

  // Get the authorization token from the request headers
  const token = req.headers.authorization;
  const user = getUsernameFromToken(token)
  // Get the IP address of the client making the request for logging
  let ip = (req.headers['x-forwarded-for'] || '').split(',').shift() || req.connection.remoteAddress;

  //
  // Healthcheck handler
  // Logs: each request is logged
  //
  if (parsedUrl.query.healthcheck && req.method === 'GET') {
    writeLog('healthcheck', { ip: ip });
    res.writeHead(200, { 'Content-Type': 'text/plain' });
    res.end('healthy');
  }
  //
  // Handler for getting all file paths from the server
  // Logs: on unauthorized and error
  //
  else if (req.method === 'GET' && parsedUrl.query.allFiles) {
    const directoryPath = path.join(__dirname, 'data');
    fs.readdir(directoryPath, (err, files) => {
      if (verifyToken(token)) {
        if (err) {
          writeLog('fetch-files-error', { ip: ip, error: err, user: user });
          res.writeHead(500, { 'Content-Type': 'text/plain' });
          res.end('Error reading files');
          return;
        }
        const filesInfo = files.map(file => {
          const filePath = path.join(directoryPath, file);
          const stats = fs.statSync(filePath);
          return {
            name: file,
            uploadedAt: stats.birthtime,
          };
        });
        res.writeHead(200, { 'Content-Type': 'application/json' });
        res.end(JSON.stringify(filesInfo));
        return;
      } else {
        writeLog('fetch-file-paths-rejected', { ip: ip, message: "request rejected - unauthorized", user: user });
        res.writeHead(401, { 'Content-Type': 'text/plain' });
        res.end('Unauthorized');
      }
    });
    return;
  } 
  //
  // Handler for downloading a single file
  // Logs: download success, error, and unauthorized
  //
  else if (req.method === 'GET' && parsedUrl.query.filename) {
    if (verifyToken(token)) {
      const filePath = path.join(__dirname, 'data', parsedUrl.query.filename);
      fs.access(filePath, fs.constants.F_OK, (err) => {
        if (err) {
          writeLog('download-file-error', { ip: ip, file: filePath, error: err, user: user  });
          res.writeHead(404, { 'Content-Type': 'text/plain' });
          res.end('File not found');
          return;
        }
        // If file exists, pipe it to response
        writeLog('download-file-success', { ip: ip, file: filePath, user: user });
        const readStream = fs.createReadStream(filePath);
        res.writeHead(200, { 'Content-Type': 'application/octet-stream' });
        readStream.pipe(res);
      });
      return;
    } else {
      writeLog('download-file-rejected', { ip: ip,  message: "request rejected - unauthorized", user: user });
      res.writeHead(401, { 'Content-Type': 'text/plain' });
      res.end('Unauthorized');
    }
  }
  //
  // Handler for downloading a set of files
  // Logs: batch download success, unauthorized, and error
  //
  else if (req.method === 'GET' && parsedUrl.query.batchFiles) {
    if (verifyToken(token)) {
      const fileNames = parsedUrl.query.batchFiles.split(',');
      const zipFileName = 'batch_download.zip';
      const zipFilePath = path.join(__dirname, 'data', zipFileName);
      const output = fs.createWriteStream(zipFilePath);
      const archive = archiver('zip');
    
      output.on('close', () => {
        res.writeHead(200, {
          'Content-Type': 'application/zip',
          'Content-Disposition': `attachment; filename=${zipFileName}`,
        });
        fs.createReadStream(zipFilePath)
        .pipe(res)
        .on('finish', () => {
          // Delete the ZIP file after it has been streamed to the response
          fs.unlink(zipFilePath, (err) => {
            if (err) {
              console.error('Error deleting ZIP file:', err);
            }
          });
        });
      });
    
      archive.on('error', (err) => {
        writeLog('batch-download-error', { ip: ip, error: err, user: user });
        res.writeHead(500, { 'Content-Type': 'text/plain' });
        res.end('Error creating ZIP archive');
      });
    
      archive.pipe(output);
    
      fileNames.forEach((fileName) => {
        const filePath = path.join(__dirname, 'data', fileName);
        archive.file(filePath, { name: fileName });
      });
      writeLog('batch-download-success', { ip: ip,  user: user, fileNames: fileNames });
      archive.finalize();
      return;
    } else {
      writeLog('batch-download-rejected', { ip: ip,  message: "request rejected - unauthorized",  user: user  })
      res.writeHead(401, { 'Content-Type': 'text/plain' });
      res.end('Unauthorized');
    }

  }
  //
  // Handler for checking validity of auth token
  // Logs: none
  //
  else if (req.method === 'GET' && parsedUrl.query.isAuth) {
    if (verifyToken(token)) {
      res.writeHead(200, { 'Content-Type': 'text/plain' });
      res.end('Authorized');
    } else {
      res.writeHead(401, { 'Content-Type': 'text/plain' });
      res.end('Unauthorized');
    }
  } 
  //
  // Handler for User Login
  // Logs: on login failed and login error
  //
  else if (req.method === 'POST' && parsedUrl.query.login) {
    let body = '';
  
    req.on('data', (chunk) => {
      body += chunk;
    });
  
    req.on('end', () => {
      const { username, password } = JSON.parse(body);
      let savedPasswordHash = process.env[username.toUpperCase()]
      try {
        if (verifyPassword(username, password, savedPasswordHash)){
          const token = jwt.sign({ username }, secretKey, { expiresIn: '1h' });
          res.writeHead(200, { 'Content-Type': 'application/json' });
          res.end(JSON.stringify({ token }));
        } else {
          writeLog('login-failed', {user: username, message: "request rejected" })
          res.writeHead(401, { 'Content-Type': 'text/plain' });
          res.end('Invalid credentials');
        }
      } catch (e) {
        writeLog('login-error', {user: username })
        res.writeHead(500, { 'Content-Type': 'text/plain' });
        res.end('Something went wrong...');
      }
    });
  }
  //
  // Handler for Uploading Files
  // Logs: on unauthorized and device upload success
  //
  else if (req.method === 'POST') {
    const parsedUrl = url.parse(req.url, true);
    const password = parsedUrl.query.password;
    const hashed_password = process.env.DEVICE

    if (!verifyDeviceSecret(password, hashed_password)) {
      writeLog('device-upload-rejected', {ip: ip, message: "request rejected" })
      res.writeHead(401, { 'Content-Type': 'text/plain' });
      res.end('Unauthorized');
      return 
    }

    const form = new formidable.IncomingForm();
    form.uploadDir = path.join(__dirname, 'tmpdata');
    form.keepExtensions = true;
    let fields = {};

    form.on('field', function(field, value) {
      fields[field] = value;
    });
  
    form.on('file', function(name, file) {
      const oldPath = file.path;
      const newPath = path.join(__dirname, 'data', file.name);
      
      fs.rename(oldPath, newPath, function(err) {
        if (err) throw err;
        console.log('Successfully moved ' + file.name);
      });
    });
  
    form.on('end', function() {
      writeLog('device-upload-success', {ip: ip})
      res.writeHead(200, { 'Content-Type': 'text/plain' });
      res.end('Files uploaded successfully');
    });
  
    form.parse(req);
  }
  //
  // Handler for Batch Deleting Files
  // Note: this is a noop, if you want to allow deletion, remove the return
  // Logs: on deletion success, deletion failure, and unauthorized
  //
  else if (req.method === 'PUT' && req.url === '/') {
    const parsedUrl = url.parse(req.url, true);
    const fileNames = parsedUrl.query.fileNames.split(',');
    const errorFiles = [];
    return; // remove to allow for deletion (given a valid JWT i.e. a valid user)
    
    if (!verifyToken(token)) {
      writeLog('authorized-file-deletion', {ip: ip, message: "request rejected"})
      res.writeHead(401, { 'Content-Type': 'text/plain' });
      res.end('Unauthorized');
      return;
    }
  
    // Delete the files
    fileNames.forEach((fileName) => {
      const filePath = path.join(__dirname, 'data', fileName);
      try {
        fs.unlinkSync(filePath);
      } catch (err) {
        console.error('Error deleting file:', err);
        errorFiles.push(fileName);
      }
    });
  
    if (errorFiles.length > 0) {
      writeLog('error-file-deletion', {ip: ip, errorFiles: errorFiles, user: user })
      res.writeHead(200, { 'Content-Type': 'application/json' });
      res.end(JSON.stringify({ success: false, errorFiles }));
    } else {
      writeLog('file-deletion-success', {ip: ip, fileNames: fileNames, user: user })
      res.writeHead(200, { 'Content-Type': 'application/json' });
      res.end(JSON.stringify({ success: true }));
    }
  } 
  //
  // Handler for retrieving the index.html file (static build of the react app)
  // Logs: on error
  //
  else if (req.url === '/') {
    const indexPath = path.join('/users/c/v/cvatech/www-root/', 'index.html');
    fs.readFile(indexPath, (err, data) => {
      if (err) {
        writeLog('page-view-error', {ip: ip, message: err})
        res.writeHead(200, { 'Content-Type': 'text/plain' });
        res.end('something wrong');
        return;
      }
      res.writeHead(200, { 'Content-Type': 'text/html' });
      res.end(data);
    });
  } 
  //
  // Handler for 404
  //
  else {
    // Respond with a 404 for any other route
    writeLog('page-404', {ip: ip})
    res.writeHead(404, { 'Content-Type': 'text/plain' });
    res.end('Error no match');

  }
}).listen();
