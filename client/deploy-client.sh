#!/bin/bash
set -e

# Navigate to the project directory
cd ./build

# Build the React app
npm run build

# Copy the build directory to the server
scp -r -oHostKeyAlgorithms=+ssh-dss ./ cvatech@w3.uvm.edu:/users/c/v/cvatech/www-root/

# Restart the prod server
ssh -oHostKeyAlgorithms=+ssh-dss cvatech@w3.uvm.edu 'silk app cvatech.w3.uvm.edu load'

